json.extract! item, :id, :Line_item, :Item, :Description, :sizes, :factor, :Price, :Qty, :Category, :created_at, :updated_at
json.url item_url(item, format: :json)