class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
    if user && User.find_by(name: params[:session][:name])
    log_in user
    redirect_to user
    else
      flash[:danger] = 'Invalid email/name combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
  	log_out
    redirect_to root_url
  end
end
