class CustomerIntialsController < ApplicationController
  before_action :set_customer_intial, only: [:show, :edit, :update, :destroy]

  # GET /customer_intials
  # GET /customer_intials.json
  def index
    @customer_intials = CustomerIntial.all

      respond_to do |format|
      format.html 
      format.csv { send_data @customer_intials.to_csv }     
      format.xlsx {response.headers['Content-Disposition'] = 'attachment; filename="all_customer.xlsx"'}
    end
  end



  # GET /customer_intials/1
  # GET /customer_intials/1.json
  def show
  end

  # GET /customer_intials/new
  def new
    @customer_intial = CustomerIntial.new
  end

  # GET /customer_intials/1/edit
  def edit
  end

  # POST /customer_intials
  # POST /customer_intials.json
  def create
    @customer_intial = CustomerIntial.new(customer_intial_params)

    
      if @customer_intial.save
    UserMailer.customer_email(@customer_intial).deliver_later  
      flash[:success] = "Welcome to The Home Depot!"
      redirect_to @customer_intial
      else
        format.html { render :new }
        format.json { render json: @customer_intial.errors, status: :unprocessable_entity }
      end
    
  end

  # PATCH/PUT /customer_intials/1
  # PATCH/PUT /customer_intials/1.json
  def update
    respond_to do |format|
      if @customer_intial.update(customer_intial_params)
        flash[:success] = "Customer intial was successfully updated."
        format.html { redirect_to @customer_intial }
        format.json { render :show, status: :ok, location: @customer_intial }
      else
        format.html { render :edit }
        format.json { render json: @customer_intial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_intials/1
  # DELETE /customer_intials/1.json
  def destroy
    @customer_intial.destroy
    respond_to do |format|
      format.html { redirect_to customer_intials_url, notice: 'Customer intial was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_intial
      @customer_intial = CustomerIntial.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_intial_params
      params.require(:customer_intial).permit(:name, :spouse_single, :address, :county, :zip, :email, :phone, :alt_phone, :appoitment_date, :time, :best_time_to_confirm, :home_owners, :two_hours, :spouse, :notes, :ISP_name, :date, :store_number)
    end
end
