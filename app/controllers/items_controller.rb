class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:new, :edit, :update, :destroy]
  #before_action :correct_item,   only: [:edit, :update]
  # GET /items
  # GET /items.json
  def index
    @user = User.new  
    

    @items = Item.all
    if params[:search]
      @items = Item.search(params[:search]).order("line_item ASC")
    else
      @items = Item.order('line_item ASC')
    end 

    respond_to do |format|
      format.html 
      format.json 
      format.js {render layout: false}
      format.csv { send_data @items.to_csv }
      format.xlsx {response.headers['Content-Disposition'] = 'attachment; filename="all_items.xlsx"'}
    end
    
  end

    

  # GET /items/1
  # GET /items/1.json
  def show
    respond_to do |format|
      format.html
      format.js {render layout: false}
    end
  end

  # GET /items/new
  def new
    @item = current_user.items.build
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    @item = current_user.items.build(item_params)

    respond_to do |format|     
      if @item.save    

        flash[:success] = "Item was successfully created"
        format.html { redirect_to @item }
        format.json { render :show, status: :created, location: @item }
        format.js {render layout: false}
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        flash[:success] = "Item was successfully updated"
        format.html { redirect_to @item }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:line_item, :item, :description, :sizes, :factor, :Price, :Qty, :category, :image, :remove_image)
    end

    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

   #def correct_item
   #  @item = Item.find(params[:id])
   #  redirect_to(root_url) unless @item.user == current_user
   #end
end
