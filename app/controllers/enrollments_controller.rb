class EnrollmentsController < ApplicationController
    
include HomeHelper

  before_action :current_user
  before_action :logged_in_user


  before_action :set_enrollment, only: [:show, :edit, :update, :destroy]

  # GET /enrollments
  # GET /enrollments.json
  def index
    @enrollments = Enrollment.all
  end

  # GET /enrollments/1
  # GET /enrollments/1.json
  def show

    @enrollments = @current_user.enrollments
    
    respond_to do |format|
      format.html 
      format.csv { send_data @enrollments.to_csv}
    end
  end

  def cart_csv

    @enrollments = @current_user.enrollments
    
    respond_to do |format|
      format.html 
      format.csv { send_data @enrollments.to_csv}
      format.xlsx {response.headers['Content-Disposition'] = 'attachment; filename="cart_items.xlsx"'}
    end
  end


  # GET /enrollments/new
  def new
    
  end

  # GET /enrollments/1/edit
  def edit
  end

  # POST /enrollments
  # POST /enrollments.json
  

  def create

    item = Item.find(params[:item_id])
    @enrollment = @current_user.add_to_list(item)

    respond_to do |format|
      if @enrollment.save
        flash[:success] = "Item was successfully added"
        format.html { redirect_to @enrollment.user # action: 'show', id: user.id
           }
      else
        format.html {render :index}

      end
    end
  end

    def create10

    item = Item.find(params[:item_id])
    @enrollment = @current_user.add_to_list10(item)

    respond_to do |format|
      if @enrollment.save
        flash[:success] = "Item was successfully added"
        format.html { redirect_to @enrollment.user # action: 'show', id: user.id
           }
      else
        format.html {render :index}

      end
    end
  end

  def remove_a_item
    item = Item.find(params[:item_id])
    @enrollment = @current_user.remove_from_list(item)
    respond_to do |format|
      if @enrollment.save
      
        flash[:danger] = "Item was successfully removed"
        format.html { redirect_to @enrollment.user }

      else
        format.html {render :index}

      end

    end
  end

   def remove_a_item10
    item = Item.find(params[:item_id])
    @enrollment = @current_user.remove_from_list10(item)
    respond_to do |format|
      if @enrollment.save
      
        flash[:danger] = "Item was successfully removed"
        format.html { redirect_to @enrollment.user }

      else
        format.html {render :index}

      end

    end
  end

  # PATCH/PUT /enrollments/1
  # PATCH/PUT /enrollments/1.json
  # original
  def update1
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to @enrollment, notice: 'Enrollment was successfully updated.' }
        format.json { render :show, status: :ok, location: @enrollment }
      else
        format.html { render :edit }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  @newQty = params[:enrollment][:quantity]
    #@cart = current_cart
    @enrollment = @current_user.enrollments.find_by_id(params[:id])
    @enrollment.quantity = @newQty
    @enrollment.save
     
    respond_to do |format|
      format.html { redirect_to @enrollment.user }
    end
    
  end

  # DELETE /enrollments/1
  # DELETE /enrollments/1.json
  def destroy
    @enrollment.destroy
    respond_to do |format|
    flash[:danger] = "The item was successfully removed"
      format.html { redirect_to @current_user }
      format.json { head :no_content }
    end
  end

  def empty_cart
    @user = User.find(params[:id])
    
      @user.enrollments.each do |u|
        u.destroy
      end

    respond_to do |format|
    flash[:danger] = "Your cart was successfully removed"
      format.html { redirect_to @current_user }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enrollment
      @enrollment = Enrollment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enrollment_params
      params.require(:enrollment).permit(:user_id, :item_id)
    end
end