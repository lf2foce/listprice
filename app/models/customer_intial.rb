class CustomerIntial < ApplicationRecord
	def self.to_csv
		attributes = %w{ISP_name name spouse_single address county zip email phone alt_phone appoitment_date time best_time_to_confirm home_owners two_hours spouse notes  date store_number}

		CSV.generate(headers: true) do |csv|
			csv << attributes

			all.each do |customer|
				csv << attributes.map{ |attr| customer.send(attr) }
			end
		end
	end
end
