class Item < ApplicationRecord

mount_uploader :image, ImageUploader

has_many :users, through: :enrollments
has_many :enrollments


	def self.search(search)	
		if search
		    #where('sizes LIKE ?', "%#{search}%")
		    where("sizes ILIKE ? OR LOWER(item) ILIKE ? OR description ILIKE ? OR category ILIKE ? OR line_item::text ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
		else
		  scoped
		end
	end


	def self.to_csv
		attributes = %w{line_item item description sizes factor Price Qty category}

		CSV.generate(headers: true) do |csv|
			csv << attributes

			all.each do |item|
				csv << attributes.map{ |attr| item.send(attr) }
			end
		end
	end

	

end
