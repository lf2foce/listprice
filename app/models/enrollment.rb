class Enrollment < ApplicationRecord
  belongs_to :user
  belongs_to :item
  
  def self.to_csv
		attributes = %w{Line_Item item description Price quantity Total}

		CSV.generate(headers: true) do |csv|
			csv << attributes

				all.each do |en|	
				total = en.item.Price.to_i * en.quantity
				csv << [en.item.line_item, en.item.item, en.item.description, en.item.Price, en.quantity, total ] 
			end
		end
	end
end
