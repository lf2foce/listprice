class User < ApplicationRecord
	has_many :items, through: :enrollments
	has_many :enrollments

	before_save {self.email = email.downcase}
	validates :name, presence: true, 
	 length: {minimum:3, maximum: 25}
	 
	 VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	 validates :email, presence: true, length: {maximum: 50},
	 format: {with: VALID_EMAIL_REGEX}

	def add_to_list(item)
		current_item = enrollments.find_by(item_id: item.id)
		if current_item
			current_item.quantity += 1
		else
			current_item = enrollments.build(item_id: item.id)
		end
		current_item
	end

	def remove_from_list(item)
		current_item = enrollments.find_by(item_id: item.id)
		if current_item && current_item.quantity > 1
		current_item.quantity -= 1
		else
			current_item.destroy
		end
		current_item
		
	end

	def remove_from_list10(item)
		current_item = enrollments.find_by(item_id: item.id)
		if current_item && current_item.quantity > 1
		current_item.quantity -= 10
		else
			current_item.destroy
		end
		current_item
		
	end

	def add_to_list10(item)
		current_item = enrollments.find_by(item_id: item.id)
		if current_item
			current_item.quantity += 10
		else
			current_item = enrollments.build(item_id: item.id)
		end
		current_item
	end

	

end
