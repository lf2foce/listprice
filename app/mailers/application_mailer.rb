class ApplicationMailer < ActionMailer::Base
  default from: 'homeservicescabinetrefacing@gmail.com'
  layout 'mailer'
end
