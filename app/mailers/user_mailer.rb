class UserMailer < ApplicationMailer
	def new_order
		@greeting = "Hi"
		mail to: "lf2foce@gmail.com"
	end

	def welcome_email(user)
	    @user = user
	    @items = Item.all
	    @url  = 'http://listprice11.herokuapp.com/login'
	    mail(to: @user.email, subject: 'Welcome to Home Depot Home Services')
	end

	def customer_email(customer_intial)	
		@customer_intial = customer_intial
		@url  = 'http://listprice11.herokuapp.com/'
	    mail(to: @customer_intial.email, subject: 'In Store Demo Scheduled', bcc: 'media@theohiohomepro.com')
	   
  	end

  	def send_price_email(user)
	    @user = user
	    @items = Item.all
	    @url  = 'http://listprice11.herokuapp.com/'
	    mail(to: @user.email, subject: "List price from HDHS for #{@user.name}")
	end

end
