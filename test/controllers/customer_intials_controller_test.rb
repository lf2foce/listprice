require 'test_helper'

class CustomerIntialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer_intial = customer_intials(:one)
  end

  test "should get index" do
    get customer_intials_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_intial_url
    assert_response :success
  end

  test "should create customer_intial" do
    assert_difference('CustomerIntial.count') do
      post customer_intials_url, params: { customer_intial: { address: @customer_intial.address, alt_phone: @customer_intial.alt_phone, appoitment_date: @customer_intial.appoitment_date, best_time_to_confirm: @customer_intial.best_time_to_confirm, county: @customer_intial.county, email: @customer_intial.email, home_owners: @customer_intial.home_owners, name: @customer_intial.name, notes: @customer_intial.notes, phone: @customer_intial.phone, spouse: @customer_intial.spouse, spouse_single: @customer_intial.spouse_single, time: @customer_intial.time, two_hours: @customer_intial.two_hours, zip: @customer_intial.zip } }
    end

    assert_redirected_to customer_intial_url(CustomerIntial.last)
  end

  test "should show customer_intial" do
    get customer_intial_url(@customer_intial)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_intial_url(@customer_intial)
    assert_response :success
  end

  test "should update customer_intial" do
    patch customer_intial_url(@customer_intial), params: { customer_intial: { address: @customer_intial.address, alt_phone: @customer_intial.alt_phone, appoitment_date: @customer_intial.appoitment_date, best_time_to_confirm: @customer_intial.best_time_to_confirm, county: @customer_intial.county, email: @customer_intial.email, home_owners: @customer_intial.home_owners, name: @customer_intial.name, notes: @customer_intial.notes, phone: @customer_intial.phone, spouse: @customer_intial.spouse, spouse_single: @customer_intial.spouse_single, time: @customer_intial.time, two_hours: @customer_intial.two_hours, zip: @customer_intial.zip } }
    assert_redirected_to customer_intial_url(@customer_intial)
  end

  test "should destroy customer_intial" do
    assert_difference('CustomerIntial.count', -1) do
      delete customer_intial_url(@customer_intial)
    end

    assert_redirected_to customer_intials_url
  end
end
