Rails.application.routes.draw do
  get 'sessions/new'

  resources :customer_intials
  resources :users

  resources :items
  resources :enrollments

  get 'cart_csv' => 'enrollments#cart_csv', as: :cart_csv

  get 'user/:id/empty_cart' => 'enrollments#empty_cart'
  get 'user/:id/remove_a_item' => 'enrollments#remove_a_item', as: :remove
  get 'user/:id/remove_a_item10' => 'enrollments#remove_a_item10', as: :remove10
  
get 'users/:id/add_to_list10' => 'enrollments#create10', as: :add_to_list10
get 'users/:id/add_to_list' => 'enrollments#create', as: :add_to_list

  get 'home/index'
  get 'home' => 'home#index'
  get 'home/xoa' => 'home#xoa_het'
  get 'user/:id/send_order' => 'users#send_price'
  
  root 'items#index'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
