# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161128064645) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "list_price_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "customer_intials", force: :cascade do |t|
    t.string   "name"
    t.string   "spouse_single"
    t.text     "address"
    t.string   "county"
    t.integer  "zip"
    t.string   "email"
    t.string   "phone"
    t.string   "alt_phone"
    t.date     "appoitment_date"
    t.time     "time"
    t.time     "best_time_to_confirm"
    t.boolean  "home_owners"
    t.boolean  "two_hours"
    t.boolean  "spouse"
    t.text     "notes"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "ISP_name"
    t.string   "date"
    t.integer  "store_number"
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "quantity",   default: 1
    t.index ["item_id"], name: "index_enrollments_on_item_id", using: :btree
    t.index ["user_id"], name: "index_enrollments_on_user_id", using: :btree
  end

  create_table "items", force: :cascade do |t|
    t.integer  "line_item"
    t.string   "item"
    t.string   "description"
    t.string   "sizes"
    t.string   "factor"
    t.float    "Price"
    t.integer  "Qty"
    t.string   "category"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "image"
    t.string   "ISP_name"
    t.date     "date"
    t.integer  "store_number"
    t.integer  "user_id"
    t.index ["user_id"], name: "index_items_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "login"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "enrollments", "items"
  add_foreign_key "enrollments", "users"
  add_foreign_key "items", "users"
end
