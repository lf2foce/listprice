class ChangeCustomerIntialsAltPhone < ActiveRecord::Migration[5.0]
  def change
  	 change_table :customer_intials do |t|
      t.change :alt_phone, :string
    end
  end
end
