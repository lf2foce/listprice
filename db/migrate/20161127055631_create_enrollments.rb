class CreateEnrollments < ActiveRecord::Migration[5.0]
  def up
    create_table :enrollments do |t|
      t.references :user, foreign_key: true, index: true
      t.references :item, foreign_key: true, index: true

      t.timestamps
    end
  end

  def down
  	drop_table :enrollments
  end
end
