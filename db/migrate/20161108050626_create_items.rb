class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.integer :Line_item
      t.string :Item
      t.string :Description
      t.string :sizes
      t.string :factor
      t.float :Price
      t.integer :Qty
      t.string :Category

      t.timestamps
    end
  end
end
