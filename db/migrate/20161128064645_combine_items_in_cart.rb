class CombineItemsInCart < ActiveRecord::Migration[5.0]
  def up
  	User.all.each do |cart|
  		sums = cart.enrollments.group(:item_id).sum(:quantity)

  		sums.each do |item_id, quantity|
  			if quantity > 1
  				cart.enrollments.where(item_id: item_id).delete_all
  				# replace
  				item = cart.enrollments.build(item_id: item_id)
  				item.quantity = quantity
  				item.save!
  			end
  		end
  	end
  end

  def down
  	Enrollment.where("quantity>1").each do |en|
  		en.quantity.times do
  			Enrollment.create(
  				user_id: en.user_id,
  				item_id: en.item_id,
  				quantity: 1
  				)
  		end
  		en.destroy
  	end
  end

end
