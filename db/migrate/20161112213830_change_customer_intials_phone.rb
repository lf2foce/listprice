class ChangeCustomerIntialsPhone < ActiveRecord::Migration[5.0]
  def up
    change_table :customer_intials do |t|
      t.change :phone, :string
    end
  end
 
  def down
    change_table :customer_intials do |t|
      t.change :phone, :integer
    end
  end
end
