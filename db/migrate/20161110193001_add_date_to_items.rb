class AddDateToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :ISP_name, :string
    add_column :items, :date, :date
    add_column :items, :store_number, :integer
  end
end
