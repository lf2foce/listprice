class AlterColumnTableItems < ActiveRecord::Migration[5.0]
  def change
  	rename_column :items, :Line_item, :line_item
  	rename_column :items, :Description, :description
  	rename_column :items, :Item, :item
  	rename_column :items, :Category, :category
  end
end
