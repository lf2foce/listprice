class CreateCustomerIntials < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_intials do |t|
      t.string :name
      t.string :spouse_single
      t.text :address
      t.string :county
      t.integer :zip
      t.string :email
      t.integer :phone
      t.integer :alt_phone
      t.date :appoitment_date
      t.time :time
      t.time :best_time_to_confirm
      t.boolean :home_owners
      t.boolean :two_hours
      t.boolean :spouse
      t.text :notes

      t.timestamps
    end
  end
end
