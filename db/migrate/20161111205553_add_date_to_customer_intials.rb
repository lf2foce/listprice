class AddDateToCustomerIntials < ActiveRecord::Migration[5.0]
  def change
    add_column :customer_intials, :ISP_name, :string
    add_column :customer_intials, :date, :string
    add_column :customer_intials, :store_number, :integer
  end
end
